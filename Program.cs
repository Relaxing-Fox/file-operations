﻿using System;
using static System.Console;
using System.IO;

namespace file_operations
{
    class Program
    {
        static void Main(string[] args)
        {
            string tempDir = Environment.GetEnvironmentVariable("TEMP");
            string srcPath = tempDir + @"\SourceDirectory";
            string destPath = tempDir +  @"\DestinationDirectory";
            string srcFileName = "test1.txt";
            string srcFile = Path.Combine(srcPath, srcFileName);

            // Creates Directory called "%temp%\SourceDirectory" if doesn't exist
            if (!Directory.Exists(srcPath))
            {
                try
                {
                    Directory.CreateDirectory(srcPath);
                    WriteLine("Source path created!");
                }
                catch (IOException e)
                {
                    WriteLine(e.Message);
                }
            }
            else
            {
                WriteLine("Source path already exists!");
            }

            // Creates File in "%temp%\SourceDirectory" called "test1.txt"
            if (!File.Exists(srcFile))
            {
                try
                {
                    File.Create(Path.Combine(srcPath, srcFileName), 1024, FileOptions.Asynchronous);
                    WriteLine("Source file created!");
                }
                catch (IOException e)
                {
                    WriteLine(e.Message);
                }
            }
            else
            {
                WriteLine("Source file already exists!");
            }

            // Creates Directory called "%temp%\DestinationDirectory" if doesn't exist
            if (!Directory.Exists(destPath))
            {
                try
                {
                    Directory.CreateDirectory(destPath);
                    WriteLine("Destination path created!");
                }
                catch (IOException e)
                {
                    WriteLine(e.Message);
                }
            }
            else
            {
                WriteLine("Destination path already exists!");
            }

            // Checks if file in "%temp%\SourceDirectory" called "test1.txt" exists to then copies the file to "%temp%\DestinationDirectory" with rename text file to be called "test1.txt.new"
            if (Directory.Exists(srcPath))
            {
                string[] files = Directory.GetFiles(srcPath);

                // Copy the files and overwrite destination files if they already exist.
                foreach (string s in files)
                {
                    // Extract the file name from the path.
                    var destFileName = Path.GetFileName(s);
                    var destFile = Path.Combine(destPath, destFileName + ".new");

                    if (File.Exists(destFile))
                    {
                        WriteLine("Destination file already exists!");
                    }
                    else
                    {
                        try
                        {
                            File.Copy(s, destFile, true);
                            WriteLine("Destination file created!");
                        }
                        catch (IOException e)
                        {
                            WriteLine(e.Message);
                        }
                    }
                }                
            }
            else
            {
                WriteLine("Source path does not exist!");
            }

            // End of program.
            WriteLine("Program finished.");
        }
    }
}

