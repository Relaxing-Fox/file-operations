# File Operations

## Introduction

This is a simple file I/O operation that renames the existing file in SourceDirectory and places it into the DestinationDirectory with a new file extension called ".new".

## Instructions
1.	Copy File from source directory to destination.
2.	Check if same file is not there before copying.
3.	Rename file, add extension as .new
        Source directory and file %TEMP%\SourceDirectory\test1.txt
        Destination directory %TEMP%\DestinationDirectory\test1.txt.new
